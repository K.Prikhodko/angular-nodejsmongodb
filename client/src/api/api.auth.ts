const rootAuthAPI = "/auth";

const LoginAPI = `/api${rootAuthAPI}/login`;
const RegisterAPI = `/api${rootAuthAPI}/register`;

export default {
  LoginAPI,
  RegisterAPI
};
