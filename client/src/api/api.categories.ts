const rootAuthAPI = "/category";

const getAllCategoriesAPI = `/api${rootAuthAPI}`;

export default {
  getAllCategoriesAPI
};
