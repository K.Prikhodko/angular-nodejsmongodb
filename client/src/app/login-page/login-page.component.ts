import { Subscription } from "rxjs";
import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { AuthService } from "@shared/services/auth.service";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { MaterialService } from "@shared/classes/material.service";

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit, OnDestroy {

  form: FormGroup;
  aSub: Subscription;

  constructor(private auth: AuthService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, Validators.minLength(6)])
    });

    this.route.queryParams.subscribe((params: Params) => {
      if(params["registered"]) {
        const message = "Can access to the system";
        MaterialService.toast(message, "success");
      } else if (params["accessDenied"]) {
        const message = "Access denied";
        MaterialService.toast(message, "error");
      } else if (params["sessionExpired"]) {
        const message = "Session expired login again";
        MaterialService.toast(message, "error");
      }
    });
  }

  ngOnDestroy(): void {
    const { aSub } = this;
    if(aSub) {
      aSub.unsubscribe();
    }
  }

  onSubmit() {
    const { form, router, auth } = this;
    const {email, password} = form.value;

    form.disable();
    this.aSub = auth.login({email, password}).subscribe(
      () => router.navigate(["/overview"]),
      ({ error: { message } }) => {
        MaterialService.toast(message, "error");
        form.enable();
      }
    );
  }
}
