import { Injectable } from "@angular/core";
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { AuthService } from "@shared/services/auth.service";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { Router } from "@angular/router";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private auth: AuthService, private router: Router) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const { auth } = this;
    if (auth.isAuthenticated()) {
      request = request.clone({
        setHeaders: {
          Authorization: auth.getToken()
        }
      });
    }
    return next.handle(request).pipe(
      catchError(
        (err: HttpErrorResponse) => this.handleAuthError(err)
      )
    );
  }

  private handleAuthError(error: HttpErrorResponse): Observable<HttpEvent<any>> {
    if(error.status === 401) {
      this.router.navigate(["/login"], {
        queryParams: {
          sessionExpired: true
        }
      });
    }
    return throwError(error);
  }
}
