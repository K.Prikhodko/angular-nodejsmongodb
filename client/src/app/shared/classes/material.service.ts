import { ElementRef } from "@angular/core";

declare var M;

export class MaterialService {
  static toast(message: string, classes: string) {
    M.toast({ html: message, classes  });
  }

  static initializeFloatingButton(ref: ElementRef) {
    M.FloatingActionButton.init(ref.nativeElement);
  }
}
