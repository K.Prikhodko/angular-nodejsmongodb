import { AfterViewInit, Component, ElementRef, ViewChild } from "@angular/core";
import { MaterialService } from "@shared/classes/material.service";
import { AuthService } from "@shared/services/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-main-layout",
  templateUrl: "./main-layout.component.html",
  styleUrls: ["./main-layout.component.scss"]
})

export class MainLayoutComponent implements AfterViewInit {

  links = [
    { url: "/overview", name: "Review" },
    { url: "/analytics", name: "Analytics" },
    { url: "/history", name: "History" },
    { url: "/new-order", name: "New order" },
    { url: "/categories", name: "Categories" },
  ];

  @ViewChild("fixedButton") floatingRef: ElementRef;

  constructor(private auth: AuthService, private router: Router) { }

  ngAfterViewInit() {
    MaterialService.initializeFloatingButton(this.floatingRef);
  }

  logout(event: Event) {
    event.preventDefault();
    this.auth.logout();
    this.router.navigate(["/login"]);
  }
}
