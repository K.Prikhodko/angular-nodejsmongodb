import { Observable } from "rxjs";
import {Injectable} from '@angular/core';
import { HttpClient } from "@angular/common/http";

import { User } from "@app/shared/interfaces";
import { AuthAPI } from "@api/index";
import { tap } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private token = null;

  constructor(private http: HttpClient) {}

  login(user: User): Observable<{token: string}> {
    return this.http.post<{token: string}>(AuthAPI.LoginAPI, user)
      .pipe(tap(
        ({token}) => {
          localStorage.setItem("auth-token", token);
          this.setToken(token);
        }
      ))
  }

  registr(user: User): Observable<User> {
    return this.http.post<User>(AuthAPI.RegisterAPI, user);
  }

  setToken(token: string) {
    this.token = token
  }

  getToken(): string {
    return this.token;
  }

  isAuthenticated(): boolean {
    return !!this.token;
  }

  logout(): void {
    this.setToken(null);
    localStorage.clear();
  }

}
