import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthLayoutComponent } from "@shared/layouts/auth-layout/auth-layout.component";
import { MainLayoutComponent } from "@shared/layouts/main-layout/main-layout.component";
import { AuthGuard } from "@shared/classes/auth.guard";
import { LoginPageComponent } from "@app/login-page/login-page.component";
import { RegisterPageComponent } from "@app/register-page/register-page.component";
import { OverviewPageComponent } from "@app/overview-page/overview-page.component";
import { CategoriesPageComponent } from "@app/categories-page/categories-page.component";
import { OrderPageComponent } from "@app/order-page/order-page.component";
import { HistoryPageComponent } from "@app/history-page/history-page.component";
import { AnalyticsPageComponent } from "@app/analytics-page/analytics-page.component";
import { CategoriesFormComponent } from "@app/categories-page/categories-form/categories-form.component";


const routes: Routes = [
  {
    path: "", component: AuthLayoutComponent, children: [
      { path: "", redirectTo: "/login", pathMatch: "full"} ,
      { path: "login", component: LoginPageComponent },
      { path: "registration", component: RegisterPageComponent }
    ]
  },
  {
    path: "",
    component: MainLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      { path: "overview", component: OverviewPageComponent },
      { path: "analytics", component: AnalyticsPageComponent },
      { path: "history", component: HistoryPageComponent },
      { path: "new-order", component: OrderPageComponent },
      { path: "categories", component: CategoriesPageComponent },
      { path: "categories/new", component: CategoriesFormComponent },
      { path: "categories/:id", component: CategoriesFormComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
