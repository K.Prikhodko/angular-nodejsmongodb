const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
const Users = require("mongoose").model("users");

const key = require("Config/token.config");

const options = {
	jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
	secretOrKey: key
};

const passportJwt = passport => {
	passport.use(
		new JwtStrategy(options, async ({ userId }, done) => {
			try {
				const user = await Users.findById(userId).select("email id");

				if (user) {
					done(null, user);
				} else {
					done(null, false);
				}
			} catch (error) {
				console.log({ error });
			}
		})
	);
};

module.exports = passportJwt;
