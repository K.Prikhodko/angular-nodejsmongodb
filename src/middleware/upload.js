const multer = require("multer");
const {
	getFileType,
	createFileName,
	createFileDestination
} = require("Utils/file");
const { maxSize } = require("Config/file.config");

const config = {
	storage: multer.diskStorage({
		destination: createFileDestination,
		filename: createFileName
	}),
	fileFilter(req, file, callback) {
		callback(null, getFileType(file.mimetype));
	},
	limits: { fileSize: maxSize }
};

module.exports = multer(config);
