const moment = require("moment");
const { fileTypes, filesPath } = require("Config/file.config");

const getFileType = fileMimeType =>
	fileTypes.includes(fileMimeType.split("/")[1]);

const createFileName = (req, { originalname }, callback) => {
	const date = moment().format("DDMMYYYY-HHmmss_SSS");
	callback(null, `${date}-${originalname}`);
};

const createFileDestination = (req, file, callback) => {
	callback(null, filesPath);
};

module.exports = { getFileType, createFileName, createFileDestination };
