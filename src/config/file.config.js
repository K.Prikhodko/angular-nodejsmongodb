const fileTypes = ["png", "jpg", "jpeg", "gif", "bmp"];
const maxSize = 1024 * 1024 * 5;
const filesPath = "/uploads/";

module.exports = { fileTypes, maxSize, filesPath };
