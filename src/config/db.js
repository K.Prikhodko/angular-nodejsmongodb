const mongoose = require("mongoose");

const localMongoDB = "mongodb://localhost:27017/angular_node_db";
const cloudMongoDB =
	"mongodb+srv://kostiantyn_prykhodko:!vSCA_uRX7y6xjB@clustercrm-81bsa.mongodb.net/crm_app";
const MONGO_URI = cloudMongoDB || localMongoDB;

// Connect to DB
mongoose
	.connect(MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true })
	.then(() => {
		console.log("MongoDB Connection Successful");
	})
	.catch(err => console.log(err));
