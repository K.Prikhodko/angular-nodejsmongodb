const authRoutes = require("Routes/auth");
const orderRoutes = require("Routes/order");
const categoryRoutes = require("Routes/category");
const positionRoutes = require("Routes/position");
const analyticsRoutes = require("Routes/analytics");
const usersRoutes = require("Routes/users");

const {
	AUTH_ROUTE,
	ORDER_ROUTE,
	CATEGORY_ROUTE,
	POSITION_ROUTE,
	ANALYTICS_ROUTE,
	USERS_ROUTE
} = require("Constants/api");

const appRoutes = router => {
	// Apply auth routes
	router.use(AUTH_ROUTE, authRoutes);
	router.use(ORDER_ROUTE, orderRoutes);
	router.use(CATEGORY_ROUTE, categoryRoutes);
	router.use(POSITION_ROUTE, positionRoutes);
	router.use(ANALYTICS_ROUTE, analyticsRoutes);
	router.use(USERS_ROUTE, usersRoutes);
	return router;
};

module.exports = appRoutes;
