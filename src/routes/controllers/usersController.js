const getAllUsers = (req, res) => {
	res.status(200).json({
		users: "get all users"
	});
};
const createUser = (req, res) => {
	res.status(200).json({
		users: "create user"
	});
};
const getUserById = (req, res) => {
	res.status(200).json({
		users: "get user by id"
	});
};
const changeUserById = (req, res) => {
	res.status(200).json({
		users: "change user by id"
	});
};
const deleteUserById = (req, res) => {
	res.status(200).json({
		users: "delete user by id"
	});
};

module.exports = {
	getAllUsers,
	createUser,
	getUserById,
	changeUserById,
	deleteUserById
};
