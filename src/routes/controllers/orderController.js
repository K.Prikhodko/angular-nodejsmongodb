const Order = require("Model/Order");
const errorHandler = require("Utils/errorHandler");

const getAllOrders = async (
	{ query: { offset, limit, start, end, order }, user: { id } },
	res
) => {
	const query = {
		user: id
	};

	if (start) query.date = { $gte: start };

	if (end) {
		if (!query.date) query.date = {};
		query.date["$lte"] = end;
	}

	if (order) query.order = +order;

	try {
		const orders = await Order.find(query)
			.sort({ date: -1 })
			.skip(+offset)
			.limit(+limit);
		res.status(200).json({ orders });
	} catch (error) {
		errorHandler(res, error);
	}
};
const createOrder = async ({ body: { list }, user: { id } }, res) => {
	try {
		const lastOrder = await Order.findOne({ user: id }).sort({ date: -1 });
		const maxOrder = lastOrder ? lastOrder.order : 0;
		const order = await new Order({
			list,
			user: id,
			order: maxOrder + 1
		}).save();
		res.status(200).json(order);
	} catch (error) {
		errorHandler(res, error);
	}
};

module.exports = {
	getAllOrders,
	createOrder
};
