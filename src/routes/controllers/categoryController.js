const Category = require("Model/Category");
const Position = require("Model/Position");
const errorHandler = require("Utils/errorHandler");

const getAllCategories = async ({ user: { id } }, res) => {
	try {
		const categories = await Category.find({ user: id });
		res.status(200).json(categories);
	} catch (error) {}
};

const createCategory = async ({ body: { name }, user: { id }, file }, res) => {
	const category = new Category({
		name,
		user: id,
		imageSrc: file ? file.path : ""
	});
	try {
		await category.save();
		res.status(200).json(category);
	} catch (error) {
		errorHandler(res, error);
	}
};

const getCategoryById = async ({ params: { id } }, res) => {
	try {
		const category = await Category.findById(id);
		res.status(200).json(category);
	} catch (error) {
		errorHandler(res, error);
	}
};

const deleteCategoryById = async ({ params: { id } }, res) => {
	try {
		await Category.remove({ _id: id });
		await Position.remove({ category: id });
		res.status(200).json({ message: "Category deleted" });
	} catch (error) {
		errorHandler(res, error);
	}
};

const changeCategoryById = async (
	{ params: { id }, body: { name }, file },
	res
) => {
	const updated = {
		name
	};
	if (file) {
		updated.imageSrc = file.path;
	}
	try {
		const category = await Category.findOneAndUpdate(
			{ _id: id },
			{ $set: updated },
			{ new: true }
		);
		res.status(200).json(category);
	} catch (error) {
		errorHandler(res, error);
	}
};

module.exports = {
	getAllCategories,
	createCategory,
	getCategoryById,
	deleteCategoryById,
	changeCategoryById
};
