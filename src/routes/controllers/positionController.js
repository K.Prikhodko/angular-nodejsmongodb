const Position = require("Model/Position");
const errorHandler = require("Utils/errorHandler");

const getPositionByCategoryId = async (
	{ params: { categoryId }, user: { id } },
	res
) => {
	try {
		const positions = await Position.find({
			category: categoryId,
			user: id
		});
		res.status(200).json(positions);
	} catch (error) {
		errorHandler(res, error);
	}
};

const createPosition = async (
	{ body: { name, cost }, user: { id }, params: { categoryId } },
	res
) => {
	try {
		const position = await Position({
			name,
			cost,
			category: categoryId,
			user: id
		}).save();
		res.status(201).json(position);
	} catch (error) {
		errorHandler(res, error);
	}
};

const changePosition = async ({ params: { id }, body }, res) => {
	try {
		const position = await Position.findOneAndUpdate(
			{ _id: id },
			{ $set: body },
			{ new: true }
		);
		res.status(200).json(position);
	} catch (error) {
		errorHandler(res, error);
	}
};

const deletePosition = async ({ params: { id } }, res) => {
	try {
		await Position.remove({
			_id: id
		});
		res.status(200).json({ message: "Position deleted" });
	} catch (error) {
		errorHandler(res, error);
	}
};

module.exports = {
	getPositionByCategoryId,
	createPosition,
	changePosition,
	deletePosition
};
