const getOverview = (req, res) => {
	res.status(200).json({
		overview: "get analytics overview"
	});
};
const getAnalytics = (req, res) => {
	res.status(200).json({
		analytics: "get analytics "
	});
};

module.exports = {
	getOverview,
	getAnalytics
};
