const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const User = require("Model/Users");
const jwtKey = require("Config/token.config");
const errorHandler = require("Utils/errorHandler");

const login = async ({ body: { email, password } }, res) => {
	const candidate = await User.findOne({ email });
	const passwordResult =
		candidate && bcrypt.compareSync(password, candidate.password);
	if (passwordResult) {
		const token = jwt.sign(
			{
				email: candidate.email,
				userId: candidate._id
			},
			jwtKey,
			{ expiresIn: 3600 }
		);
		res
			.status(200)
			.json({ message: "Sign In Successfully", token: `Bearer ${token}` });
	} else {
		res.status(404).json({ message: "User not found" });
	}
};

const register = async ({ body: { email, password } }, res) => {
	const candidate = await User.findOne({ email });

	if (candidate) {
		res.status(409).json({ message: "User already exists" });
	} else {
		const salt = bcrypt.genSaltSync(10);
		const user = new User({ email, password: bcrypt.hashSync(password, salt) });

		try {
			await user.save();
			res.status(201).json({ message: `User ${email} created` });
		} catch (error) {
			errorHandler(res, error);
		}
	}
};

module.exports = {
	login,
	register
};
