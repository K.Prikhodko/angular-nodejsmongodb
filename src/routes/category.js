const express = require("express");
const passport = require("passport");
const upload = require("Middleware/upload");
const {
	getAllCategories,
	createCategory,
	getCategoryById,
	changeCategoryById,
	deleteCategoryById
} = require("Routes/controllers/categoryController");

const category = express.Router();

const { ID, MAIN } = require("Constants/api").PARAMS;

//------------- Get All Categories
// http://localhost:3000/api/category - GET
category.get(
	MAIN,
	passport.authenticate("jwt", { session: false }),
	getAllCategories
);

//-------------- Create Category
// http://localhost:3000/api/category - POST
category.post(
	MAIN,
	passport.authenticate("jwt", { session: false }),
	upload.single("image"),
	createCategory
);

//-------------- Get Category By ID
// http://localhost:3000/api/category/:id - GET
category.get(
	ID,
	passport.authenticate("jwt", { session: false }),
	getCategoryById
);

//--------------- Change Category By ID
// http://localhost:3000/api/category/:id - PATCH
category.patch(
	ID,
	passport.authenticate("jwt", { session: false }),
	upload.single("image"),
	changeCategoryById
);

//----------------- Delete Category By ID
// http://localhost:3000/api/category/:id - DELETE
category.delete(
	ID,
	passport.authenticate("jwt", { session: false }),
	deleteCategoryById
);

module.exports = category;
