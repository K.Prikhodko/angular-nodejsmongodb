const express = require("express");
const passport = require("passport");
const order = express.Router();
const {
	getAllOrders,
	createOrder
} = require("Routes/controllers/orderController");

const { MAIN } = require("Constants/api").PARAMS;

// http://localhost:3000/api/order?offset={val}&limit={val} - GET
order.get(MAIN, passport.authenticate("jwt", { session: false }), getAllOrders);

// http://localhost:3000/api/order - POST
order.post(MAIN, passport.authenticate("jwt", { session: false }), createOrder);

module.exports = order;
