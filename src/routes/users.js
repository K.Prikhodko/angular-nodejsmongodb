const express = require("express");
const users = express.Router();
const {
	getAllUsers,
	createUser,
	getUserById,
	changeUserById,
	deleteUserById
} = require("Routes/controllers/usersController");

const { MAIN, ID } = require("Constants/api").PARAMS;

// http://localhost:3000/api/users - GET
users.get(MAIN, getAllUsers);

// http://localhost:3000/api/users - POST
users.post(MAIN, createUser);

// http://localhost:3000/api/users/:id - GET
users.get(ID, getUserById);

// http://localhost:3000/api/users/:id - PATCH
users.patch(ID, changeUserById);

// http://localhost:3000/api/users/:id - DELETE
users.delete(ID, deleteUserById);

module.exports = users;
