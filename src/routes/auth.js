const express = require("express");
const auth = express.Router();
const { login, register } = require("Routes/controllers/authController");

const { LOGIN, REGISTER } = require("Constants/api").PARAMS;

// http://localhost:3000/api/auth/login - POST
auth.post(LOGIN, login);

// http://localhost:3000/api/auth/register - POST
auth.post(REGISTER, register);

module.exports = auth;
