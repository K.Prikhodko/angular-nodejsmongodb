const express = require("express");
const analytics = express.Router();
const {
	getOverview,
	getAnalytics
} = require("Routes/controllers/analyticsController");

const { MAIN, OVERVIEW } = require("Constants/api").PARAMS;

// http://localhost:3000/api/analytics - GET
analytics.get(MAIN, getAnalytics);

// http://localhost:3000/api/analytics/overview - GET
analytics.get(OVERVIEW, getOverview);

module.exports = analytics;
