const express = require("express");
const passport = require("passport");
const position = express.Router();
const {
	createPosition,
	getPositionByCategoryId,
	changePosition,
	deletePosition
} = require("Routes/controllers/positionController");

const { MAIN, ID } = require("Constants/api").PARAMS;

// http://localhost:3000/api/position - POST
position.post(
	MAIN,
	passport.authenticate("jwt", { session: false }),
	createPosition
);

// http://localhost:3000/api/position/:categoryId - GET
position.get(
	ID,
	passport.authenticate("jwt", { session: false }),
	getPositionByCategoryId
);

// http://localhost:3000/api/position/:id - PATCH
position.patch(
	ID,
	passport.authenticate("jwt", { session: false }),
	changePosition
);

// http://localhost:3000/api/position/:id - DELETE
position.delete(
	ID,
	passport.authenticate("jwt", { session: false }),
	deletePosition
);

module.exports = position;
