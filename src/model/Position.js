const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const positionSchema = new Schema({
	name: {
		type: String,
		required: true
	},
	cost: {
		type: Number,
		required: true
	},
	user: {
		ref: "users",
		type: Schema.Types.ObjectId
	},
	category: {
		ref: "categories",
		type: Schema.Types.ObjectId
	}
});

const positionModel = mongoose.model("positions", positionSchema);

module.exports = positionModel;
