const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const Schema = mongoose.Schema;

const UserSchema = new Schema({
	username: { type: String, lowercase: true, require: true, unique: true },
	password: { type: String, require: true },
	email: { type: String, require: true, unique: true },
});

UserSchema.pre("save", function (next) {
	const user = this;
	bcrypt.hash(user.password, 10, (err, hash) => {
		if (err) return next(err);
		user.password = hash;
		next();
	});
});

module.exports = mongoose.model("User", UserSchema);
