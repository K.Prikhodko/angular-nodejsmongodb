const PORT = process.env.PORT || 3000;
const HTTP_TYPE = "http";
const DOMAIN = "localhost";

const APP_ENV_DEV = "dev";

module.exports = {
	PORT,
	HTTP_TYPE,
	DOMAIN,
	APP_ENV_DEV
};
