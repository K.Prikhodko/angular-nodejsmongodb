const ROOT_PATH = "/api";
const AUTH_ROUTE = "/auth";
const ORDER_ROUTE = "/order";
const CATEGORY_ROUTE = "/category";
const POSITION_ROUTE = "/position";
const ANALYTICS_ROUTE = "/analytics";
const USERS_ROUTE = "/users";

const PARAMS = {
	MAIN: "/",
	ID: "/:id",
	OVERVIEW: "/overview",
	LOGIN: "/login",
	REGISTER: "/register"
};

module.exports = {
	ROOT_PATH,
	AUTH_ROUTE,
	ORDER_ROUTE,
	CATEGORY_ROUTE,
	POSITION_ROUTE,
	ANALYTICS_ROUTE,
	USERS_ROUTE,
	PARAMS
};
