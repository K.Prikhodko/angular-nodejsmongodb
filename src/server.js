const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const passport = require("passport");
const { ROOT_PATH } = require("Constants/api");
const { PORT, HTTP_TYPE, DOMAIN, APP_ENV_DEV } = require("Constants/env");

require("./config/db");

const app = express();
const router = express.Router();

const appRoutes = require("./routes")(router);

// Apply cors requests
app.use(cors());

// Add passport.js to make simple authentication
app.use(passport.initialize());
require("Middleware/passport")(passport);

// Log the entry routes
app.use(morgan(APP_ENV_DEV));

// Make uploads folder as static to be able to get files from browser
app.use("/uploads", express.static(__dirname + "/uploads"));

// Parse request body
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Create root path for routing
app.use(ROOT_PATH, appRoutes);

// Start server
app.listen(PORT, () => {
	console.log(`server is running ${HTTP_TYPE}://${DOMAIN}:${PORT}`);
});
